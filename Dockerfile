FROM php:8.1-apache

USER root

# Set working directory
WORKDIR /var/www/html

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    git \
    curl \
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libgd-dev \
    jpegoptim optipng pngquant gifsicle \
    libzip-dev \
    libonig-dev \
    libxml2-dev \
    curl \
    nano \
    zip \
    unzip \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd

# Copy existing application directory contents
COPY . /var/www/html
COPY .docker/vhost.conf /etc/apache2/sites-available/000-default.conf
# RUN Mod a2enmod
RUN a2enmod rewrite

# Install extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl gd bcmath zip

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer install
RUN chmod 777 /var/www/html/storage/logs/
RUN php artisan key:generate
RUN chmod 777 /var/www/html/storage/framework/sessions/
RUN chmod 777 /var/www/html/storage/framework/views/
